﻿namespace Calculator.Dto
{
    public class MathOperationData
    {
        public int firstOperand { get; set; }
        public int secondOperand { get; set; }
        public string operationType { get; set; }
    }
}
