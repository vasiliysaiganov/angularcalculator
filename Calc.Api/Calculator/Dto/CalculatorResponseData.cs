﻿namespace Calculator.Dto
{
    public class CalculatorResponseData
    {
        public int? calculationResult { get; set; }
        public string errorMessage { get; set; }
    }
}
