﻿using System;
using Calculator.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Calculator.Controllers
{
    [Produces("application/json")]
    [Route("api/Calculator")]
    public class CalculatorController : Controller
    {
        public string Get()
        {
            return "It works!";
        }

        [HttpPost]
        public CalculatorResponseData Post([FromBody]MathOperationData data)
        {
            var first = data.firstOperand;
            var second = data.secondOperand;
            try
            {
                var operation = (OperationType)int.Parse(data.operationType);
                switch (operation)
                {
                    case OperationType.Sum:
                        return new CalculatorResponseData {calculationResult = first + second };
                        break;
                    case OperationType.Minus:
                        return new CalculatorResponseData { calculationResult = first - second };
                        break;
                    case OperationType.Multiply:
                        return new CalculatorResponseData { calculationResult = first * second };
                        break;
                    case OperationType.Division:
                        return new CalculatorResponseData { calculationResult = first / second };
                        break;
                    default:
                        throw new ArgumentException("Cant support this operation yet!");
                }
            } catch (DivideByZeroException ex)
            {
                //TODO: log
                return new CalculatorResponseData { calculationResult = null, errorMessage = "Divide by zero exception. Try again!" };
            } catch (Exception ex)
            {
                //TODO: log
                return new CalculatorResponseData { calculationResult = null, errorMessage = "Something wrong! Try again!" };
            }
        }
    }

    public enum OperationType
    {
        Sum,
        Minus,
        Multiply,
        Division
    }
}