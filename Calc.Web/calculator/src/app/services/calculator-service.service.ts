import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CalculatorServiceService {

  api = "http://localhost:5001/api/calculator";

  constructor(private http: HttpClient) { }

  //TODO: extract to dto
  getResultOfCalculation(firstOperand: number, secondOperand: number, operationType: string): any{
    const data = {
      firstOperand,
      secondOperand,
      operationType
    }
    return (this.http.post(this.api, data));
  }
}
