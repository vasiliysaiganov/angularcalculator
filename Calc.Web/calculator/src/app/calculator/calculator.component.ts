import { Component, OnInit } from '@angular/core';

import { CalculatorServiceService } from './../services/calculator-service.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent {

  selectedOperation: string;
  firstOperand: number;
  secondOperand: number;  
  result: number;
  errorMessage: string;

  operations: Array<Object> = [
    { code: 0, name: "Sum" },
    { code: 1, name: "Minus" },
    { code: 2, name: "Multiply" },
    { code: 3, name: "Division" },
  ];

  constructor(private calculatorService: CalculatorServiceService) { }


  calculate() {
    this.calculatorService
      .getResultOfCalculation(this.firstOperand, this.secondOperand, this.selectedOperation)
      .subscribe(data => {
          this.result = data.calculationResult;
          this.errorMessage = data.errorMessage;
      })
  }
}
